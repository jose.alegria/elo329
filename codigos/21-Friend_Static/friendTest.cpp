#include <iostream>
#include "CVector.h"
using namespace std;

int main () {
  CVector a (3,5);
  CVector b (1,2);
  CVector c;
  c = a + b;
  cout << "c = " << c << endl;
  c = 5.0*c;
  cout << "5.0*c = " << c << endl;
  return 0;
}
